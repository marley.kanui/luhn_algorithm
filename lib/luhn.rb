module Luhn
  def self.is_valid?(number)
# define the 'self.is_valid?' method and set it to expect one argument 'number'
    times_two = []
# Create a variable called 'times_two' and set it equal to an empty array

    sequence = number.to_s.split('').map(&:to_i)
# Create a variable called 'sequence' and set it equal to the affected 
# 'number' argument 

# then manipulate the number argument via the '.to_s' ruby method, which  
# first changes the integer to a string

# then the '.split' ruby method, which separates each character into it's 
# own individual string (with '' as the separator)

# then the '.map' ruby mehtod ( whichreferences the ':to_i' ruby method) 
# to placeall individual characters back into an array and convert each 
# string into an integer

    sequence.reverse.each_with_index do |integer, place|
# Now reverse the 'sequence' variable with the '.reverse' ruby method 

# then use the '.each_with_index' ruby method to work with both the 
# individual integer in the 'sequence' variable as well as the numerical 
# placement (index) of each integer, and store the respective information in
# the 'integer' and 'place' variables

      if place % 2 == 0
# Start a conditional statement saying that if the modulo 2 of the 'place' 
# variable is equal to 0
        times_two << integer*2
# Then push it's respective 'integer' variable, multiplied by 2, into the 
# 'times_two' array 
      else
# Otherwise
        times_two << integer
# push the respective 'integer' variable into the 'times_two' array unaffected 
      end
# End the conditional statement
    end
# End the 'each_with_index' loop 

    times_two.map do |sub_nine|
# Now use the '.map' ruby method to affect the now filled 'times_two' array 
# and store the information in the 'sub_nine' variable
      if sub_nine >= 10
# Start a conditional statement saying that if 'sub_nine' is greater than or 
# eqaul to 10
        sub_nine - 9
# Then subtract 9 from the 'sub_nine' variable in that instance
      end
# End the conditional statement
    end
# End the '.map' loop




    added = times_two.inject(:+)
# Create a variable called 'added' and set it equal to the 'times_two' 
# array, which is being affected by the '.inject' ruby method (using the '+' 
# operator), to sum all of the contents in the 'times_two' array together

    return added % 10 == 0
# Finally return the boolean vaule stating whether the 'added' variable, 
#  modulo 10, is equal to 0 or not
  end
# End the 'self.is_valid?' method 
end
# End the 'Luhn' module
